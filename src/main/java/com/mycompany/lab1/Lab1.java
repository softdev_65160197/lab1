/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author informatics
 */
import java.util.Scanner;
public class Lab1 {
    
    Scanner scanner = new Scanner(System.in);
    private static int SIZE = 3;
    private static char EMP = ' ';
    private char[][] board;
    private char curPlayer;

    public Lab1() {
        board = new char[SIZE][SIZE];
        curPlayer = ' ';
        mainBoard();
    }
    
    private void mainBoard() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                board[i][j] = EMP;
            }
        }
    }
    
    private void printBoard() {
        System.out.println("------------");
        for (int i = 0; i < SIZE; i++) {
            System.out.print("| ");
            for (int j = 0; j < SIZE; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println("\n------------");
        }
    }
    //full
    private boolean Full() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (board[i][j] == EMP) {
                    return false;
                }
            }
        }
        return true;
    }
    
    //check
    private boolean wMove(int row, int col) {
         //row
         if (board[row][0] == board[row][1] && board[row][1] == board[row][2]) {
                return true;
         }
         //col
         if (board[0][col] == board[1][col] && board[1][col] == board[2][col]) {
                return true;
         }
         // \
        if (row == col && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
            return true;
        }

        // reverse \
        if (row + col == SIZE - 1 && board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
            return true;
        }         
         return false;           
    }
            
    private void Move(int row, int col) {
         board[row][col] = curPlayer;
    }   
    private boolean JustMove(int row, int col) {
        if (row < 0 || row >= SIZE || col < 0 || col >= SIZE || board[row][col] != EMP) {
            return false;
        }
        return true;
    }

    private void sPlayer() {  //สลับผู้เล่น
        curPlayer = (curPlayer == 'X') ? 'O' : 'X';
    }

   public void play() {
        System.out.print("Choose your XO: ");
        String Text = scanner.nextLine().toUpperCase();

        if (Text.equals("X")) {
            curPlayer = 'X';
        } else if (Text.equals("O")) {
            curPlayer = 'O';
        } else {
            System.out.println("Invalid ! , Exit.");
            return;
        }
        
        System.out.println("Let's start");
        printBoard();
        
        while (true) { // input position
            System.out.print("Player " + (curPlayer == 'X' ? "X" : "O") + ", enter your move (row column): ");
            int row = scanner.nextInt();
            int col = scanner.nextInt();

            if (JustMove(row, col)) {
                Move(row, col);
                printBoard();

                if (wMove(row, col)) {
                    System.out.println("Player " + curPlayer + " wins!");
                    break;
                }

                if (Full()) {
                    System.out.println("It's a draw!");
                    break;
                }
                sPlayer();
            } else {
                System.out.println("Try again.");
            }
        }

        scanner.close();
    }
    public static void main(String[] args) {
        Lab1 game = new Lab1();
        game.play();
    }
        
}
